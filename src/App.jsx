import React from 'react';
import './App.css';
import { BrowserRouter as Switch, Route, Redirect } from 'react-router-dom';
import Header from './app/header/Header';
import Home from './app/home/Home';
import Raiding from './app/raiding/Raiding';
import Roster from './app/roster/Roster';
import Join from './app/join/Join';

export const BASE_URL = window.location.hostname === "www.thegodhand.com" || "thegodhand.com" ? "" : "/the-god-hand";

const App = () => 
<div className="App">
  <Switch>
    <div className="content">
      <Header/>
      <div className="content-body">
        <Route exact path={`${BASE_URL}/`} component={Home}/>
        <Route path={`${BASE_URL}/raiding`} component={Raiding}/>
        <Route path={`${BASE_URL}/roster`} component={Roster}/>
        <Route path={`${BASE_URL}/join`} component={Join}/>
        <Redirect to={`${BASE_URL}/`}/>
      </div>
    </div>
  </Switch>
</div>

export default App;
