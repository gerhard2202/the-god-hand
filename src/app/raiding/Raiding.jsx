import React from 'react';
import './Raiding.css';
import raidingInfo from '../../assets/raiding.json';
import expectations from '../../assets/expectations.json';
import playerScreenshots from '../shared/playerScreenshots';

const imageStyle = {
  backgroundImage: `url(${playerScreenshots["Spaghettidrk"]})`,
  backgroundSize: 'cover'
}

const Raiding = () =>
<div className="raiding">
  <header className="raiding-title">
    <h1 className="font-smoke">The Raid Team</h1>
    <h2 className="font-smoke">Quick Info</h2>
  </header>
  <section className="raiding-info font-smoke">
    <div className="raiding-info-chart">
      {
        Object.keys(raidingInfo).map((key) =>
          <div className="raiding-info-text-row font-white">
            <div className="raiding-info-label">{key}</div>
            <div className="raiding-info-text">{raidingInfo[key]}</div>
          </div>
        )
      }
    </div>
    <div style={imageStyle} className="raiding-info-image"/>
  </section>
  <section className="raiding-details font-smoke">
    <header className="raiding-title">
      <h1 className="font-smoke">Team Info</h1>
      <h2>Expectations</h2>
    </header>
    <section className="raiding-details-split">
      <div className="raiding-details-split-column">
        <header className="raiding-title">
          <h3>During</h3>
        </header>
        <ol>
          {
            expectations.during.map((entry) =>
              <li>{entry}</li>
            )
          }
        </ol>
      </div>
      <div className="raiding-details-split-column">
        <header className="raiding-title">
          <h3>Before & After</h3>
        </header>
        <ol>
          {
            expectations.outside.map((entry) =>
              <li>{entry}</li>
            )
          }
        </ol>
      </div>
    </section>
    <section className="raiding-details-whole">
      <header className="raiding-title">
        <h2>Raid Night</h2>
      </header>
    </section>
    <p className="raiding-details-p">
      While there is a list of expectations above, it is more of a guideline to ensure raid night goes smoothly.
      Raids are typically a pretty laid back environment with plenty of goofing around, gambling, and having a good time.
      We want fast clears, Mythic kills, and Cutting Edge, but those things aren't worth getting if the road there isn't fun.
    </p>
    <p className="raiding-details-p">
      So come to raid with your fights watched and your flasks ready, take down bosses, roll some dice, and leave with someone
      else's best-in-slot weapon. Our team isn't one that's going to bicker about who trades what loot to whom or bark at someone
      because they didn't pay some sort of guild vault tithe. We're making it to Cutting Edge as a team that stands together.
    </p>
  </section>
</div>

export default Raiding;