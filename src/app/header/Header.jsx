import React from 'react';
import './Header.css';
import emblem from '../../assets/horde.png';
// eslint-disable-next-line no-unused-vars
import { BrowserRouter as Router, Route, Link, withRouter } from 'react-router-dom'; // For some reason this works with the unused Router being imported and doesn't otherwise
import { BASE_URL } from '../../App';

const headerEmblemStyle = {
  margin: '0.5rem 0 0.5rem 1rem',
  width: '2rem',
  backgroundImage: `url(${emblem})`,
  backgroundSize: 'cover'
}

const HeaderLink = ({label, to, activeOnlyWhenExact }) =>
  <Route
    path={to}
    exact={activeOnlyWhenExact}
    children={( {match} ) => (
      <Link to={to} className={match ? "header-link header-link-active" : "header-link"}>{label}</Link>
    )}
  >
  </Route>

const routes = [
  '/',
  '/raiding',
  '/roster',
  '/join'
]

const findLeftRoute = path => routes[Math.max(0, routes.findIndex(route => route === path) - 1)];
const findRightRoute = path => routes[Math.min(routes.length - 1, routes.findIndex(route => route === path) + 1)];

const Header = ({location}) =>
<header className="header font-smoke">
    <nav className="header-nav">
      <div className="header-block header-left">
        <div style={headerEmblemStyle} className="logo-container"></div>
        <div>
          <HeaderLink to={`${BASE_URL}/`} activeOnlyWhenExact={true} label={"The God Hand"}/>
        </div>
      </div>
      <div className="header-block header-right">
        <div className="dir-nav">
          <HeaderLink to={`${BASE_URL}${findLeftRoute(location.pathname.replace(BASE_URL, ""))}`} label="<"/>
          <HeaderLink to={`${BASE_URL}${findRightRoute(location.pathname.replace(BASE_URL, ""))}`} label=">"/>
        </div>
        <ul className="right-nav">
          <li>
            <HeaderLink to={`${BASE_URL}/raiding`} label="Raiding"/>
          </li>
          <li>
            <HeaderLink to={`${BASE_URL}/roster`} label="Roster"/>
          </li>
          <li>
            <HeaderLink to={`${BASE_URL}/join`} label="Join"/>
          </li>
        </ul>
     </div>
    </nav>
</header>

export default withRouter(Header);