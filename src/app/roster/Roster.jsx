import React from 'react';
import * as moment from 'moment';
import './Roster.css';
import rosterData from '../../assets/roster.json';
import RosterCard from './RosterCard/RosterCard';
import RosterDetails from './RosterDetails/RosterDetails';
import { compose, withState, withHandlers } from 'recompose';

const IndexGuard = ({roster, index, children}) => roster[index] ? children : null;

const rosterDate = moment(rosterData.date);

const baseRoster = ({member, getCardOnClick}) =>
<div className="roster">
  <header className="roster-title">
    <h1 className="font-smoke">
      Core Roster
    </h1>
    <h2 className="font-light">
      Up to date as of {rosterDate.format("dddd, MMMM Do")}
    </h2>
  </header>
  <div className="roster-grid">
    {rosterData.roster.map((entry, index, roster) => index % 2 !== 0 ? null :
      <div key={index} className="roster-block">
        <IndexGuard roster={roster} index={index}>
          <RosterCard odd={false} member={roster[index]} onClick={getCardOnClick(roster[index])}/>
        </IndexGuard>
        <IndexGuard roster={roster} index={index+1}>
          <RosterCard odd={true} member={roster[index+1]} onClick={getCardOnClick(roster[index+1])}/>
        </IndexGuard>
      </div>  
    )}
  </div>
  <RosterDetails member={member}/>
</div>

const Roster = compose(
  withState('member', 'setMember', null),
  withHandlers({
    'getCardOnClick': ({setMember}) => (name) => (event) => setMember(name)
  })
)(baseRoster);

export default Roster;