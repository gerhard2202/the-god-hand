import React from 'react';
import screenshots from '../../shared/playerScreenshots';
import none from '../../../assets/player/none.png';
import './RosterDetails.css';

const getDetailsHeader = (member) => {
  if (member && member.name !== "???") return `${member.name} the ${member.spec} ${member.class}`;
  return "Select a member of the team.";
}

const getDetailsScreenshot = (member) => {
  if (!member || member.name === "???") return null;
  const lower = member.name.toLowerCase();
  return <div className="roster-details-screenshot-wrapper">
    <div className="roster-details-screenshot">
      {
        Object.keys(screenshots).find((key) => key === lower) ?
        <img src={screenshots[lower]} alt={member.name}/> :
        <img src={none} alt={none} />
      }
    </div>
  </div>
}

const RosterDetails = ({ member }) =>
<div className="roster-details">
  <header className="roster-details-title font-smoke">
    {getDetailsHeader(member)}
  </header>
  <div className="roster-details-body font-smoke">
    {getDetailsScreenshot(member)}
  </div>
</div>

export default RosterDetails;