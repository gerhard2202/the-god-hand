import React from 'react';
import './RosterCard.css';
import classIcons from '../../shared/classIcons';
import roleIcons from '../../shared/roleIcons';

const roleImageStyle = ( role ) => {
  return {
    backgroundImage: `url(${roleIcons[role]})`,
    backgroundSize: 'cover',
  }
}

const classImageStyle = ( cls ) => {
  return {
    backgroundImage: `url(${classIcons[cls]})`,
    backgroundSize: 'cover'
  }
};

const RosterCard = ({ odd, member, onClick }) =>
<div className="roster-card" onClick={ onClick }>
  <div className="roster-image-wrapper">
    <div style={ odd ? classImageStyle(member.class) : roleImageStyle(member.role) } className="roster-image"/>
  </div>
  <div className="roster-text font-white">
    {member.name}
  </div>
  <div className="roster-image-wrapper">
    <div style={ odd ? roleImageStyle(member.role) : classImageStyle(member.class) } className="roster-image"/>
  </div>
</div>

export default RosterCard;