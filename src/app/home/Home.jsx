import React from 'react';
import './Home.css';
import splash from '../../assets/splash.png';
import splashWebM from '../../assets/splash.webm';
import splashMP4 from '../../assets/splash.mp4';

const Home = () => 
<div className="home">
  <section className="landing">
    <div className="landing-image-wrapper">
      <video playsInline autoPlay muted loop poster={splash} className="landing-image">
        <source src={splashWebM} type="video/webm"/>
        <source src={splashMP4} type="video/mp4"/>
      </video>
    </div>
    <div className="landing-block landing-right">
      <div className="landing-right-text">
        <h1>Guts. Glory. The God Hand.</h1>
        <p>
          Fight alongside a diverse group sporting over a decade of experience clearing bosses and taking down foes in the arena.
          From pushing keystones to conquering the Alliance on the battlefield, we do it all.
          Join us and come along on one of the wildest rides Azeroth has to offer.
        </p>
      </div>
    </div>
  </section>
</div>

export default Home;