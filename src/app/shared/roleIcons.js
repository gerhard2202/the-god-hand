import healer from '../../assets/role/healer.png';
import dps from '../../assets/role/dps.png';
import tank from '../../assets/role/tank.png';

export default {
  "Healer": healer,
  "Damage": dps,
  "Tank": tank
}