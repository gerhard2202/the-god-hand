import spaghettidrk from '../../assets/player/spaghettidrk.png';
import craing from '../../assets/player/craing.jpg';
import spinnaz from '../../assets/player/spinnaz.png';
import slan from '../../assets/player/slan.jpg';
import demonfruits from '../../assets/player/demonfruits.jpg';
import memebeams from '../../assets/player/memebeams.png';

export default {
  spaghettidrk,
  craing,
  spinnaz,
  demonfruits,
  slan,
  memebeams
}