import dk from '../../assets/class/death-knight.png';
import dh from '../../assets/class/demon-hunter.png';
import druid from '../../assets/class/druid.png';
import hunter from '../../assets/class/hunter.png';
import mage from '../../assets/class/mage.png';
import monk from '../../assets/class/monk.png';
import paladin from '../../assets/class/paladin.png';
import priest from '../../assets/class/priest.png';
import rogue from '../../assets/class/rogue.png';
import shaman from '../../assets/class/shaman.png';
import warlock from '../../assets/class/warlock.png';
import warrior from '../../assets/class/warrior.png';

export default {
  "Death Knight": dk,
  "Demon Hunter": dh,
  "Druid": druid,
  "Hunter": hunter,
  "Mage": mage,
  "Monk": monk,
  "Paladin": paladin,
  "Priest": priest,
  "Rogue": rogue,
  "Shaman": shaman,
  "Warlock": warlock,
  "Warrior": warrior
}