import React from 'react';
import './BattleTag.css';
import battleNetBadge from '../../../assets/battle-net.png';

const badgeStyle = {
  'backgroundImage': `url(${battleNetBadge})`,
  'backgroundSize': 'cover'
}

const BattleTag = ({ tag, label, labelColor="#444", inverse=false }) => {
  const components = [];
  components.push(<div style={badgeStyle} className="battle-tag-badge"/>);
  components.push(<div className="battle-tag-tag">{tag}</div>);
  components.push(<div style={{ 'color': labelColor }} className="battle-tag-label">{label}</div>);

  return <div className="battle-tag">
    {
      inverse ? 
        components.reverse().map((component) => component) :
        components.map((component) => component)
    }
  </div>
}

export default BattleTag;