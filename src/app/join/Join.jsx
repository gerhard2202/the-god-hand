import React from 'react';
import './Join.css';
import BattleTag from './battletag/BattleTag';

const Join = () =>
<div className="join">
  <header className="join-title font-smoke">
    <h1>Join The God Hand</h1>
  </header>
  <section className="join-banners">
    <BattleTag tag="Gerhard#1676" label="Memebeams" labelColor={"#FF7D0A"}/>
    <BattleTag tag="SpaghettiDrk#1230" label="Spaghettidrk" labelColor={"#C41F3B"} />
  </section>
  <section className="join-body font-smoke">
    <section class="join-section">
      <header className="join-title">
        <h2>Join the Guild</h2>
      </header>
      <p className="join-body-p">
        Want to join the guild? Feel free! We don't have any restrictions or rites of passage for anyone who'd like to join the guild and start participating in PvP and M+.
        <br/>
        Just type <code>/who The God Hand</code> to find someone who is online in the guild and ask for an invite! You can also whisper Memebeams or Spaghettidrk
        directly, or contact them through the BattleTags listed above.
      </p>
    </section>
    <section class="join-section">
      <header className="join-title">
        <h2>Join the Team</h2>
      </header>
      <p className="join-body-p">
        While you're free to join the guild, joining the team is a little tougher - we're focused on forming a team of core raiders that can pull their weight
        and make our raid times dependably.
        <br/>
        If you think you've got what it takes, reach out to Spaghettidrk or Memebeams to discuss how you might fit into the
        roster and then tag along on the next raid night that we've got room to show us what you've got!
      </p>
    </section>
  </section>
</div>

export default Join;